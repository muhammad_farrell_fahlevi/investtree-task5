package com.investree.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "transaksis")
public class Transaksi implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "tenor")
    private Integer tenor;
    @Column(name = "total_pinjaman")
    private Double total_pinjaman;
    @Column(name = "bunga_persen")
    private Double bunga_persen;
    @Column(name = "status")
    private String status;
    @JsonIgnore
    @OneToMany(mappedBy = "transaksi", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PaymentHistory> paymentHistoryList;
    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_peminjam")
    private User userPeminjam;
    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_meminjam")
    private User userMeminjam;
}
