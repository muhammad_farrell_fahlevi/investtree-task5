package com.investree.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "username", unique = true, nullable = false)
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "is_active")
    private boolean is_active;
    @OneToOne(mappedBy = "user")
    private UserDetail userDetail;
    @JsonIgnore
    @OneToMany(mappedBy = "userPeminjam", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transaksi> transaksiPeminjam;
    @JsonIgnore
    @OneToMany(mappedBy = "userMeminjam", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transaksi> transaksiMeminjam;
}
